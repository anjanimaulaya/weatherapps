package com.example.weather.activity

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.androdocs.navigationdrawer.R
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_weather.*
import org.json.JSONObject

class WeatherActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var toolbar: Toolbar
    lateinit var requestQueue: RequestQueue
    lateinit var stringRequest: StringRequest

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weather)
        requestQueue = Volley.newRequestQueue(this)
        search_button.setOnClickListener(this)

        toolbar = findViewById(R.id.weather_toolbar)
        setSupportActionBar(toolbar)

        // Display icon in the toolbar
        supportActionBar!!.setDisplayShowHomeEnabled(true);
        supportActionBar!!.setDisplayUseLogoEnabled(true);
        supportActionBar!!.setDisplayHomeAsUpEnabled(true);

        // Remove default title text
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        //Back button
        toolbar.setNavigationOnClickListener {onBackPressed() }

    }

    override fun onClick(v: View?) {
        when(v){
            search_button -> getWeather()
        }
    }

    fun getWeather() {

        val city = input_city_text.text
        val country = input_country_text.text
        val API_KEY = "fd82985a61e52ec6f7aad8360db9da01"

        //build request
        stringRequest = StringRequest(Request.Method.GET,

            "http://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${API_KEY}&units=metric",
            Response.Listener { response ->

                var stringResponse = response.toString()
                val jsonObj = JSONObject(stringResponse)

                val  main: JSONObject = jsonObj.getJSONObject("main")
                val sys: JSONObject = jsonObj.getJSONObject("sys")
                val windObj: JSONObject = jsonObj.getJSONObject("wind")
                val cloud: JSONObject = jsonObj.getJSONObject("clouds")

                val city: String = jsonObj.getString("name")
                val country: String = sys.getString("country")
                val temp: String = main.getString("temp")
                val minTemp: String = main.getString("temp_min")
                val maxTemp: String = main.getString("temp_max")
                val humidity: String = main.getString("humidity")
                val wind: String = windObj.getString("speed")
                val clouds: String = cloud.getString("all")
                val pressure: String = main.getString("pressure")

                city_view.text = "${city}, ${country}"
                temp_desc_view.text = "$temp C"
                temp_title_view.text = "Temperature"
                temp_view.text = ": $minTemp C - $maxTemp C"
                humidity_title_view.text = "Humidity"
                humidity_view.text = ": $humidity %"
                wind_title_view.text = "Wind"
                wind_view.text = ": $wind m/s"
                clouds_title_view.text = "Clouds"
                clouds_view.text = ": $clouds %"
                pressure_title_view.text = "Pressure"
                pressure_view.text = ": $pressure hpa"

            },

            Response.ErrorListener {
                city_view.text = "Not found"
                temp_desc_view.text = ""
                temp_title_view.text = ""
                temp_view.text = ""
                humidity_title_view.text = ""
                humidity_view.text = ""
                wind_title_view.text = ""
                wind_view.text = ""
                clouds_title_view.text = ""
                clouds_view.text = ""
                pressure_title_view.text = ""
                pressure_view.text = ""
            })

        println("stringRequest " + stringRequest + " ini")

        //add request
        requestQueue.add(stringRequest)

        println("requestQueue " + requestQueue + " ini")

    }
}
